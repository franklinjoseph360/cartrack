const breeds = require('../data');

const getBreeds = (req, res) => {
    try {
        res.json({
            data: breeds,
            ip: req.ip,
            request: req.request
        })
    } catch (err) {
        console.error(`Get All Orders error: ${err}`);
        res.status(400).send('Oops something went wrong. Please contact the API service provider');
    }
};

module.exports = { getBreeds }