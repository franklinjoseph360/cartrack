require('dotenv').config();
const path = require('path');
const express = require('express');
const app = express();
const routes = require('./routes');

app.use(express.static(path.resolve(__dirname, '../client/build')));

app.use(express.json());

const SERVER_PORT = process.env.SERVER_PORT || 9000

app.use('/api/', routes);

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/build', 'index.html'));
});

app.listen(SERVER_PORT, () => {
    console.log(`App running on port: ${SERVER_PORT}`)
})

module.exports = app;