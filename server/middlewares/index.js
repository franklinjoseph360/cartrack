require('dotenv').config();
var Utilities = require('../utilities');

const MAX_REQUESTS = process.env.MAX_USER_REQUESTS || 10
const TTL_EXPIRY_IN_SECONDS = process.env.TTL_EXPIRY_IN_SECONDS || 600

const limitRequests = () => {
    var requests = {}
    return async function (req, res, next) {
        let ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
        requests = {
            ...requests,
            [ip]: {
                ...requests[ip],
                attempts: requests[ip]?.attempts ? requests[ip].attempts + 1 : 1
            }
        }
        if (requests[ip].attempts > MAX_REQUESTS && !Utilities.isExpired(requests[ip].expiry)) {
            console.error(`${new Date()} BLOCKED: ${ip} - Request_No: ${requests[ip].attempts} | Expiry: ${requests[ip].expiry}`);
            res.status(429).json({
                error: "Too many requests"
            })
        } else {
            if (requests[ip].attempts > MAX_REQUESTS && Utilities.isExpired(requests[ip].expiry)) {
                requests[ip].attempts = 1;
            }
            if (requests[ip].attempts === 1) {
                requests[ip].expiry = Utilities.setExpiry(TTL_EXPIRY_IN_SECONDS);
            }
            console.log(`${new Date()} SUCCESS: ${ip} - Request_No: ${requests[ip].attempts} | Expiry: ${requests[ip].expiry}`);
            req.ip = ip;
            req.request = {
                attempts: requests[ip].attempts,
                expiry: new Date(requests[ip].expiry)
            };
            next();
        }
    }
}

const registerRequest = limitRequests();

module.exports = { registerRequest }