const express = require('express');
const Router = express.Router();
const AppController = require('../controllers/');
const Middleware = require('../middlewares/');

Router.use((req, res, next) => Middleware.registerRequest(req, res, next));

Router.get('/breeds', AppController.getBreeds);

module.exports = Router