const setExpiry = (seconds) => {
    let timeNow = new Date();
    return timeNow.setSeconds(seconds);
}

const isExpired = (expiry) => {
    let rightNow = new Date().getTime();
    if (rightNow > expiry) {
        return true;
    }
    else {
        return false;
    }
}

module.exports = { setExpiry, isExpired }