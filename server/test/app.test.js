const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../server');

const should = chai.should();

chai.use(chaiHttp);

describe("API =>", () => {
    describe("/api/breeds", () => {
        it("Should return list of breeds", (done) => {
            chai.request(app)
                .get('/api/breeds')
                .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('data');
                res.body.should.have.property('ip');
                res.body.should.have.property('request');
                done();
            });
        })
    })
})