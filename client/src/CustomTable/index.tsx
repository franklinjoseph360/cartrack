import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { TableHeader } from './TableHeader';
import { TableBody } from './TableBody';
import axios from 'axios';

const Table = styled.table`
    width: 100%;
    border-collapse: collapse;
    width: 100%;
    border-radius: 8px;
    text-align: left;
    border: 1px solid grey;
    background-color: white;
    box-shadow: 5px 5px 5px grey;
`;

const Error = styled.div`
    color: red;
    font-weight: bold;
`;

export interface Breed {
    id: number;
    name: string;
    breed_group?: string;
    bred_for?: string;
    life_span?: string;
    temperament?: string;
}

interface Error {
    config: any;
    data: {
        error: string;
    };
    status: number;
    statusText: string;
}

export type BreedType = Breed[];

const axiosFetch = (url: string) => {
    const promise = axios.get(url);
    return promise;
};

export const TableWrapper: React.FC = () => {
    const [breeds, setBreeds] = useState<BreedType>([]);
    const [error, setError] = useState<Error>();

    useEffect(() => {
        axiosFetch('http://127.0.0.1:9000/api/breeds')
            .then((res: { data: { data: any } }) => {
                const breeds = res.data.data;
                setBreeds(breeds);
            })
            .catch((err: { response: React.SetStateAction<Error | undefined> }) => {
                setError(err.response);
            });
    }, []);

    const handleReload = () => {
        axiosFetch('http://127.0.0.1:9000/api/breeds')
            .then((res: { data: { data: any } }) => {
                const breeds = res.data.data;
                setBreeds(breeds);
            })
            .catch((err: { response: React.SetStateAction<Error | undefined> }) => {
                setError(err.response);
            });
    };

    console.log('error', error);

    return (
        <div>
            {error && <Error>{error.statusText}</Error>}
            <button onClick={() => handleReload()}>Reload</button>
            <Table>
                <TableHeader />
                <TableBody breeds={breeds} />
            </Table>
        </div>
    );
};
