import React from 'react';
import styled from 'styled-components';
import { Breed, BreedType } from './index';

const Td = styled.td`
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    height: 40px;
    padding: 8px;
`;

const Tr = styled.tr`
    &:hover {
        background-color: 'lightgrey';
    }
`;

interface BreedProps {
    breed: Breed;
}

const Tbody: React.FC<BreedProps> = ({ breed }) => {
    return (
        <Tr>
            <Td>{breed.id}</Td>
            <Td>{breed.id}</Td>
            <Td>{breed.name}</Td>
            <Td>{breed.breed_group}</Td>
            <Td>{breed.bred_for}</Td>
            <Td>{breed.life_span}</Td>
            <Td>{breed.temperament}</Td>
        </Tr>
    );
};

interface ListProps {
    breeds: BreedType;
}

const TableBody: React.FC<ListProps> = (props) => {
    if (!props.breeds) return null;
    console.log('breed', props.breeds);
    return (
        <tbody>
            {props.breeds.map((d: Breed) => {
                return <Tbody breed={d} key={d.id} />;
            })}
        </tbody>
    );
};

export { TableBody, Tbody };
