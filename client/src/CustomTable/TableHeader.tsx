import React from 'react';
import styled from 'styled-components';

const Th = styled.th`
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    padding: 8px;
    height: 50px;
`;

interface Label {
    name: string;
    label: string;
    sort: boolean;
}

const columns: Label[] = [
    {
        name: 'id',
        label: 'Id',
        sort: false,
    },
    {
        name: 'name',
        label: 'Name',
        sort: true,
    },
    {
        name: 'breed_group',
        label: 'Breed Group',
        sort: true,
    },
    {
        name: 'bred_for',
        label: 'Bred For',
        sort: false,
    },
    {
        name: 'life_span',
        label: 'Life Span',
        sort: false,
    },
    {
        name: 'temperament',
        label: 'Temperament',
        sort: false,
    },
];

export const TableHeader: React.FC = () => {
    return (
        <thead>
            <tr>
                {columns.map((column: { label: any }, i: React.Key | null | undefined) => {
                    return (
                        <React.Fragment key={i}>
                            <Th>{column.label}</Th>
                        </React.Fragment>
                    );
                })}
            </tr>
        </thead>
    );
};
