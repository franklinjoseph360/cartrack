import React from 'react';
import { TableWrapper } from './CustomTable';

function App() {
    return (
        <div className="App">
            <TableWrapper />
        </div>
    );
}

export default App;
