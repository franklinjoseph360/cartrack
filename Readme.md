# Cartrack Coding Challenge

For the position of Full stack developer

## Authors

Franklin Joseph  
[LinkedIn](https://www.linkedin.com/in/franklin-joseph)

## Description

A simple App to demonstrate IP throttling using Node js (express js) backend and React js Front-end

## Getting Started

### Dependencies

* Must have node and npm installed.
* Supports windows, mac and linux based systems.

### Setup or Clone Repository
* Run the following command in your terminal or command prompt:
```
> git clone https://franklinjoseph360@bitbucket.org/franklinjoseph360/cartrack.git
```
* Add .env file and add the following or use the .env.sample (optional)
```
SERVER_PORT=9000
MAX_USER_REQUESTS=3
TTL_EXPIRY_IN_SECONDS=60
```

### Run the app
Run the following commands to start the app: (to build  client and start server)
```
> npm run build
> npm start
```

Run the following command to run tests:
```
> npm test
```

Run the following command to run lint check:
```
> npm run lint
```

Run the following command to rebuild and restart server:
```
> npm run restart
```

## To see the results

* API Endpoint
[/breeds](http://127.0.0.1:9000/api/breeds)
* React UI
[/home](http://127.0.0.1:9000)


## Version History
* 1.0.0
    * Initial Release